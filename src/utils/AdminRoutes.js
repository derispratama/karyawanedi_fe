import React from 'react'
import { Navigate, Outlet } from 'react-router-dom';

export default function AdminRoutes() {
    let role = localStorage.getItem('role');
    let registered = localStorage.getItem('registered');
    let id = localStorage.getItem('id');
    return (
        role == 'ADMIN' ? <Outlet /> : <Navigate to={'/karyawan/create'} />
    )
}
