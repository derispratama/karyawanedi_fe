import React from 'react'
import { Navigate, Outlet } from 'react-router-dom';

export default function UserRoute() {
    let registered = localStorage.getItem('registered');
    return (
        registered == 'yes' ? <Outlet /> : <Navigate to={'/karyawan/create'} />
    )
}
