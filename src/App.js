import logo from './logo.svg';
import './App.css';
import { Card, CardContent, Container, Grid } from '@mui/material';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import CreateKaryawan from './components/CreateKaryawan';
import EditKaryawan from './components/EditKaryawan';
import ListKaryawan from './components/ListKaryawan';
import ShowKaryawan from './components/ShowKaryawan';
import Login from './components/Login';
import Register from './components/Register';
import PrivateRoute from './utils/PrivateRoute';
import AdminRoutes from './utils/AdminRoutes';
import UserRoute from './utils/UserRoutes';

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path='/' element={<Login />} />
        <Route path='/register' element={<Register />} />
        <Route element={<PrivateRoute />}>
          <Route element={<AdminRoutes />}>
            <Route path='/karyawan' element={<ListKaryawan />} />
            <Route path='/karyawan/:id/show' element={<ShowKaryawan />} />
          </Route>
          <Route path='/karyawan/create' element={<CreateKaryawan />} />
          <Route element={<UserRoute />}>
            <Route path='/karyawan/:id/edit' element={<EditKaryawan />} />
          </Route>
        </Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
