import React, { useState } from 'react'
import { Card, CardContent, Checkbox, Container, FormControl, FormControlLabel, FormGroup, FormLabel, Grid, InputLabel, MenuItem, Paper, Radio, RadioGroup, Select, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, TextField, Typography } from '@mui/material'
import { DemoContainer } from '@mui/x-date-pickers/internals/demo';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import Button from '@mui/material/Button';
import { styled } from '@mui/material/styles';
import { useNavigate } from 'react-router-dom';
import dayjs from 'dayjs';
import Swal from 'sweetalert2'
import axios from 'axios';

export default function CreateKaryawan() {
    const navigate = useNavigate();
    const token = localStorage.getItem("token");
    const email = localStorage.getItem("email");
    const id = localStorage.getItem("id");
    const registered = localStorage.getItem('registered');

    console.log(registered);

    let [rowsDataPendidikan, setRowsDataPendidikan] = useState([]);
    let [rowsDataPelatihan, setRowsDataPelatihan] = useState([]);
    let [rowsDataPekerjaan, setRowsDataPekerjaan] = useState([]);

    const [datas, setDatas] = useState({
        posisi: '',
        name: '',
        ktp: '',
        email: email,
        tempat: '',
        tgl_lahir: dayjs(),
        jk: 'laki-laki',
        telp: '',
        penghasial_diharapkan: '',
        agama: 'islam',
        alamat_ktp: '',
        alamat_tinggal: '',
        status: '',
        skill: '',
        penghasial_diharapkan: '',
        gol_darah: 'A',
        org_dekat: '',
        bersedia: '',
        jenjang_pendidikan: {
            name: [],
            nama_institusi: [],
            jurusan: [],
            tahun_lulus: [],
            ipk: [],
        },
        riwayat_pelatihan: {
            name: [],
            sertifikat: [],
            tahun: [],
        },
        riwayat_pekerjaan: {
            name: [],
            posisi_terakhir: [],
            pendapatan_terakhir: [],
            tahun: [],
        }
    });

    const [error, setError] = useState({
        posisi: {
            error: false,
            message: '',
        },
        penghasial_diharapkan: {
            error: false,
            message: '',
        },
        gol_darah: {
            error: false,
            message: '',
        },
        org_dekat: {
            error: false,
            message: '',
        },
        name: {
            error: false,
            message: '',
        },
        ktp: {
            error: false,
            message: '',
        },
        email: {
            error: false,
            message: '',
        },
        tempat:
        {
            error: false,
            message: '',
        },
        tgl_lahir:
        {
            error: false,
            message: '',
        },
        jk:
        {
            error: false,
            message: '',
        },
        telp:
        {
            error: false,
            message: '',
        },
        agama:
        {
            error: false,
            message: '',
        },
        alamat_ktp:
        {
            error: false,
            message: '',
        },
        alamat_tinggal:
        {
            error: false,
            message: '',
        },
        status:
        {
            error: false,
            message: '',
        },
        skill: {
            error: false,
            message: '',
        },
        bersedia: {
            error: false,
            message: '',
        },
    });

    const handleChange = (e) => {
        const { name, value, type, checked } = e.target;

        if (type == 'checkbox') {
            if (checked) {
                setDatas(values => ({ ...values, [name]: 'YA' }));
            } else {
                setDatas(values => ({ ...values, [name]: 'TIDAK' }));
            }
        } else {
            setDatas(values => ({ ...values, [name]: value }));
        }

    }

    const handleSubmit = (e) => {
        e.preventDefault();
        Swal.fire({
            title: "Do you want to save the changes?",
            showCancelButton: true,
            icon: "warning",
            confirmButtonText: "Save",
        }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            Swal.showLoading();
            if (result.isConfirmed) {
                requestApi()
            }
        });
    }

    const requestApi = async () => {
        const data = {
            'id_user': id,
            'name': datas.name,
            'ktp': datas.ktp,
            'email': datas.email,
            'tempat': datas.tempat,
            'jk': datas.jk,
            'telp': datas.telp,
            'alamat_tinggal': datas.alamat_tinggal,
            'alamat_ktp': datas.alamat_ktp,
            'posisi': datas.posisi,
            'agama': datas.agama,
            'status': datas.status,
            'skill': datas.skill,
            'penghasial_diharapkan': datas.penghasial_diharapkan,
            'gol_darah': datas.gol_darah,
            'org_dekat': datas.org_dekat,
            'bersedia': datas.bersedia,
            'tgl_lahir': dayjs(datas.tgl_lahir).format('YYYY-MM-DD'),
            'jenjang_pendidikan': datas.jenjang_pendidikan,
            'riwayat_pelatihan': datas.riwayat_pelatihan,
            'riwayat_pekerjaan': datas.riwayat_pekerjaan,
        }

        await axios.post('http://127.0.0.1:8000/api/karyawan', data, {
            headers: {
                'accept': 'application/json',
                'Accept-Language': 'en-US,en;q=0.8',
                'Authorization': `Bearer ${token}`
            }
        })
            .then((res) => {
                localStorage.setItem("registered", 'yes');
                Swal.hideLoading()
                Swal.fire("Saved!", "", "success");
                navigate('/karyawan/' + res.data.data.id + '/edit');
            })
            .catch((err) => {
                // Swal.hideLoading()
                let errorku = { ...error };
                for (const errs in errorku) {
                    errorku[errs]['error'] = false;
                    errorku[errs]['message'] = '';
                }

                for (const errs in err.response.data.errors) {
                    if (errorku[errs]) {
                        errorku[errs]['error'] = true;
                        errorku[errs]['message'] = err.response.data.errors[errs][0];
                    }
                }
                setError(errorku)
            });
    }

    const handleLogout = async () => {
        await axios.post(`http://127.0.0.1:8000/api/logout`, {
            headers: {
                'accept': 'application/json',
                'Accept-Language': 'en-US,en;q=0.8',
                'Authorization': `Bearer ${token}`
            }
        })
            .then((res) => {
                localStorage.removeItem('token')
                localStorage.removeItem('email')
                localStorage.removeItem('id')
                Swal.hideLoading()
                Swal.fire("Logout!", "", "success");
                navigate('/');
            })
            .catch((err) => {
                console.error(err);
            });
    }

    const handleAddRowsPendidikan = () => {
        const data = { ...datas };

        setRowsDataPendidikan([
            ...rowsDataPendidikan,
            { name: '', nama_institusi: '', jurusan: '', tahun_lulus: '', ipk: '' }
        ]);

        data.jenjang_pendidikan.name.push('');
        data.jenjang_pendidikan.nama_institusi.push('');
        data.jenjang_pendidikan.jurusan.push('');
        data.jenjang_pendidikan.tahun_lulus.push('');
        data.jenjang_pendidikan.ipk.push('');

        setDatas(data);
    };

    const handleAddRowsPelatihan = () => {
        const data = { ...datas };

        setRowsDataPelatihan([
            ...rowsDataPelatihan,
            { name: '', sertifikat: '', tahun: '' }
        ]);

        data.riwayat_pelatihan.name.push('');
        data.riwayat_pelatihan.sertifikat.push('');
        data.riwayat_pelatihan.tahun.push('');

        setDatas(data);
    };

    const handleAddRowsPekerjaan = () => {
        const data = { ...datas };

        setRowsDataPekerjaan([
            ...rowsDataPekerjaan,
            { name: '', posisi_terakhir: '', pendapatan_terakhir: '', tahun: '' }
        ]);

        data.riwayat_pekerjaan.name.push('');
        data.riwayat_pekerjaan.posisi_terakhir.push('');
        data.riwayat_pekerjaan.pendapatan_terakhir.push('');
        data.riwayat_pekerjaan.tahun.push('');

        setDatas(data);
    };

    const handleDeleteRowsPendidikan = (i) => {
        const deleteVal = [...rowsDataPendidikan];
        const post = { ...datas };
        if (i === 'last') {
            deleteVal.pop();
            post.jenjang_pendidikan.name.pop();
            post.jenjang_pendidikan.nama_institusi.pop();
            post.jenjang_pendidikan.jurusan.pop();
            post.jenjang_pendidikan.tahun_lulus.pop();
            post.jenjang_pendidikan.ipk.pop();
        } else {
            deleteVal.splice(i, 1);
            post.jenjang_pendidikan.name.splice(i, 1);
            post.jenjang_pendidikan.nama_institusi.splice(i, 1);
            post.jenjang_pendidikan.jurusan.splice(i, 1);
            post.jenjang_pendidikan.tahun_lulus.splice(i, 1);
            post.jenjang_pendidikan.ipk.splice(i, 1);
        }
        setRowsDataPendidikan(deleteVal);
        setDatas(post);
    }

    const handleDeleteRowsPelatihan = (i) => {
        const deleteVal = [...rowsDataPelatihan];
        const post = { ...datas };
        if (i === 'last') {
            deleteVal.pop();
            post.riwayat_pelatihan.name.pop();
            post.riwayat_pelatihan.sertifikat.pop();
            post.riwayat_pelatihan.tahun.pop();
        } else {
            deleteVal.splice(i, 1);
            post.riwayat_pelatihan.name.splice(i, 1);
            post.riwayat_pelatihan.sertifikat.splice(i, 1);
            post.riwayat_pelatihan.tahun.splice(i, 1);
        }
        setRowsDataPelatihan(deleteVal);
        setDatas(post);
    }

    const handleDeleteRowsPekerjaan = (i) => {
        const deleteVal = [...rowsDataPekerjaan];
        const post = { ...datas };
        if (i === 'last') {
            deleteVal.pop();
            post.riwayat_pekerjaan.name.pop();
            post.riwayat_pekerjaan.posisi_terakhir.pop();
            post.riwayat_pekerjaan.pendapatan_terakhir.pop();
            post.riwayat_pekerjaan.tahun.pop();
        } else {
            deleteVal.splice(i, 1);
            post.riwayat_pekerjaan.name.splice(i, 1);
            post.riwayat_pekerjaan.posisi_terakhir.splice(i, 1);
            post.riwayat_pekerjaan.pendapatan_terakhir.splice(i, 1);
            post.riwayat_pekerjaan.tahun.splice(i, 1);
        }
        setRowsDataPekerjaan(deleteVal);
        setDatas(post);
    }

    const handleChangeInputDetailPendidikan = (e, index) => {
        const { name, value } = e.target;
        const post = { ...datas };
        const datap = [...rowsDataPendidikan];

        post.jenjang_pendidikan[name][index] = value;
        datap[index][name] = value;

        setDatas(post);
        setRowsDataPendidikan(datap);
    }

    const handleChangeInputDetailPelatihan = (e, index) => {
        const { name, value } = e.target;
        const post = { ...datas };
        const datap = [...rowsDataPelatihan];

        post.riwayat_pelatihan[name][index] = value;
        datap[index][name] = value;

        setDatas(post);
        setRowsDataPelatihan(datap);
    }

    const handleChangeInputDetailPekerjaan = (e, index) => {
        const { name, value } = e.target;
        const post = { ...datas };
        const datap = [...rowsDataPekerjaan];

        post.riwayat_pekerjaan[name][index] = value;
        datap[index][name] = value;

        setDatas(post);
        setRowsDataPekerjaan(datap);
    }

    return (
        <Container>
            <form noValidate autoComplete='off' onSubmit={handleSubmit}>
                <Card style={{ marginTop: 100, marginBottom: 20, padding: 10 }}>
                    <CardContent style={{ padding: 10 }}>
                        <Grid container spacing={2}>
                            <Grid item xs={6}>
                                <Typography mb={5} gutterBottom variant="h5" component="div">
                                    Data Pribadi Pelamar
                                </Typography>
                            </Grid>
                            <Grid item xs={6}>
                                <Button onClick={handleLogout} style={{ float: 'right', marginBottom: 20, marginRight: 10, textAlign: 'center' }} variant="contained">Logout</Button>
                            </Grid>
                        </Grid>

                        <Grid container spacing={2}>
                            <Grid item xs={6}>
                                <TextField
                                    style={{ width: '100%' }}
                                    error={error.posisi.error}
                                    value={datas.posisi}
                                    name='posisi'
                                    id="outlined-basic"
                                    label="Posisi Yang Dilamar"
                                    variant="outlined"
                                    helperText={error.posisi.message}
                                    onChange={handleChange}
                                />
                            </Grid>
                            <Grid item xs={6}>
                                <TextField
                                    style={{ width: '100%' }}
                                    error={error.name.error}
                                    name='name'
                                    value={datas.name}
                                    id="outlined-basic"
                                    label="Nama"
                                    variant="outlined"
                                    helperText={error.name.message}
                                    onChange={handleChange}
                                />
                            </Grid>
                            <Grid item xs={6}>
                                <TextField
                                    onKeyPress={(event) => {
                                        if (!/[0-9]/.test(event.key)) {
                                            event.preventDefault();
                                        }
                                    }}
                                    style={{ width: '100%' }}
                                    error={error.ktp.error}
                                    value={datas.ktp}
                                    name='ktp'
                                    id="outlined-basic"
                                    label="No KTP"
                                    variant="outlined"
                                    helperText={error.ktp.message}
                                    onChange={handleChange}
                                />
                            </Grid>
                            <Grid item xs={6}>
                                <TextField
                                    style={{ width: '100%' }}
                                    error={error.status.error}
                                    name='status'
                                    id="outlined-basic"
                                    label="Status"
                                    variant="outlined"
                                    helperText={error.status.message}
                                    onChange={handleChange}
                                />
                            </Grid>
                            <Grid item xs={8}>
                                <TextField
                                    style={{ width: '100%' }}
                                    error={error.tempat.error}
                                    name='tempat'
                                    id="outlined-basic"
                                    label="Tempat Lahir"
                                    variant="outlined"
                                    helperText={error.tempat.message}
                                    onChange={handleChange}
                                />
                            </Grid>
                            <Grid item xs={4}>
                                <LocalizationProvider dateAdapter={AdapterDayjs} >
                                    <DemoContainer components={['DatePicker']} sx={{ marginTop: -1, width: '100%' }}>
                                        <DatePicker
                                            label="Tanggal Lahir"
                                            name='tgl_lahir'
                                            value={datas.tgl_lahir}
                                            onChange={(v) => setDatas(values => ({ ...values, 'tgl_lahir': v }))}
                                        />
                                    </DemoContainer>
                                </LocalizationProvider>
                            </Grid>
                            <Grid item xs={6}>
                                <FormControl fullWidth>
                                    <InputLabel id="demo-simple-select-label">Agama</InputLabel>
                                    <Select
                                        labelId="demo-simple-select-label"
                                        id="demo-simple-select"
                                        name="agama"
                                        value={datas.agama}
                                        label="Agama"
                                        onChange={handleChange}
                                    >
                                        <MenuItem value={'islam'}>Islam</MenuItem>
                                        <MenuItem value={'kristen'}>Kristen</MenuItem>
                                        <MenuItem value={'hindu'}>Hindu</MenuItem>
                                    </Select>
                                    {
                                        error.agama.error == true && (
                                            <Typography color={'error'} style={{ fontSize: 12 }}>
                                                {error.agama.message}
                                            </Typography>
                                        )
                                    }
                                </FormControl>
                            </Grid>
                            <Grid item xs={6}>
                                <FormControl>
                                    <FormLabel id="demo-radio-buttons-group-label">Jenis Kelamin</FormLabel>
                                    <RadioGroup
                                        aria-labelledby="demo-radio-buttons-group-label"
                                        defaultValue="laki-laki"
                                        name="jk"
                                        value={datas.jk}
                                        onChange={handleChange}
                                        row
                                    >
                                        <FormControlLabel value="laki-laki" control={<Radio />} label="Laki-laki" />
                                        <FormControlLabel value="perempuan" control={<Radio />} label="Perempuan" />
                                    </RadioGroup>
                                </FormControl>
                            </Grid>
                            <Grid item xs={6}>
                                <TextField
                                    error={error.alamat_ktp.error}
                                    helperText={error.alamat_ktp.message}
                                    style={{ width: '100%' }}
                                    id="outlined-multiline-static"
                                    label="Alamat KTP"
                                    name='alamat_ktp'
                                    multiline
                                    rows={4}
                                    onChange={handleChange}
                                />
                            </Grid>
                            <Grid item xs={6}>
                                <TextField
                                    error={error.alamat_tinggal.error}
                                    helperText={error.alamat_tinggal.message}
                                    style={{ width: '100%' }}
                                    id="outlined-multiline-static"
                                    label="Alamat Tinggal"
                                    name='alamat_tinggal'
                                    multiline
                                    rows={4}
                                    onChange={handleChange}
                                />
                            </Grid>
                            <Grid item xs={6}>
                                <TextField
                                    style={{ width: '100%' }}
                                    error={error.email.error}
                                    name='email'
                                    value={datas.email}
                                    id="outlined-basic"
                                    label="Email"
                                    variant="outlined"
                                    helperText={error.email.message}
                                    onChange={handleChange}
                                />
                            </Grid>
                            <Grid item xs={6}>
                                <TextField
                                    onKeyPress={(event) => {
                                        if (!/[0-9]/.test(event.key)) {
                                            event.preventDefault();
                                        }
                                    }}
                                    style={{ width: '100%' }}
                                    error={error.telp.error}
                                    helperText={error.telp.message}
                                    name='telp'
                                    id="outlined-basic"
                                    label="No Telp/HP"
                                    variant="outlined"
                                    onChange={handleChange} />
                            </Grid>
                            <Grid item xs={6}>
                                <TextField
                                    style={{ width: '100%' }}
                                    error={error.org_dekat.error}
                                    name='org_dekat'
                                    id="outlined-basic"
                                    label="Orang Yang Dapat Dihubungi"
                                    variant="outlined"
                                    helperText={error.org_dekat.message}
                                    onChange={handleChange}
                                />
                            </Grid>
                            <Grid item xs={6}>
                                <FormControl fullWidth>
                                    <InputLabel id="demo-simple-select-label">Golongan Darah</InputLabel>
                                    <Select
                                        labelId="demo-simple-select-label"
                                        id="demo-simple-select"
                                        name="gol_darah"
                                        value={datas.gol_darah}
                                        label="Golongan Darah"
                                        onChange={handleChange}
                                    >
                                        <MenuItem value={'O'}>O</MenuItem>
                                        <MenuItem value={'A'}>A</MenuItem>
                                        <MenuItem value={'AB'}>AB</MenuItem>
                                        <MenuItem value={'B'}>B</MenuItem>
                                    </Select>
                                    {
                                        error.agama.error == true && (
                                            <Typography color={'error'} style={{ fontSize: 12 }}>
                                                {error.agama.message}
                                            </Typography>
                                        )
                                    }
                                </FormControl>
                            </Grid>
                            <Grid item xs={6}>
                                <TextField
                                    onKeyPress={(event) => {
                                        if (!/[0-9]/.test(event.key)) {
                                            event.preventDefault();
                                        }
                                    }}
                                    style={{ width: '100%' }}
                                    error={error.penghasial_diharapkan.error}
                                    helperText={error.penghasial_diharapkan.message}
                                    name='penghasial_diharapkan'
                                    id="outlined-basic"
                                    label="penghasial_diharapkan Diharapkan"
                                    variant="outlined"
                                    onChange={handleChange} />
                            </Grid>
                            <Grid item xs={12}>
                                <InputLabel id="demo-simple-select-label">Bersedia Ditempatkan Di Seluruh Kantor Perusahaan</InputLabel>
                                <FormGroup row>
                                    <FormControlLabel control={<Checkbox name="bersedia" value='YA' onChange={handleChange} />} label="Saya bersedia" />
                                </FormGroup>
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    error={error.skill.error}
                                    helperText={error.skill.message}
                                    style={{ width: '100%' }}
                                    id="outlined-multiline-static"
                                    label="Skill"
                                    name='skill'
                                    multiline
                                    rows={4}
                                    onChange={handleChange}
                                />
                            </Grid>
                        </Grid>
                    </CardContent>
                </Card>
                <Card style={{ marginTop: 10, marginBottom: 10, padding: 10 }}>
                    <CardContent style={{ padding: 10 }}>
                        <Typography mb={5} gutterBottom variant="h5" component="div">
                            Pendidikan Terakhir
                        </Typography>
                        <Grid container spacing={2}>
                            <Grid item xs={12}>
                                <Button onClick={handleAddRowsPendidikan} style={{ marginTop: 10, marginRight: 10 }} variant="contained">Tambah Pendidikan</Button>
                                <TableContainer component={Paper} style={{ marginTop: 10 }}>
                                    <Table sx={{ minWidth: 650 }} aria-label="simple table">
                                        <TableHead>
                                            <TableRow>
                                                <TableCell>Jenjang Pendidikan</TableCell>
                                                <TableCell>Nama Instusi</TableCell>
                                                <TableCell>Jurusan</TableCell>
                                                <TableCell>Tahun Lulus</TableCell>
                                                <TableCell>IPK</TableCell>
                                                <TableCell>Action</TableCell>
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>
                                            {
                                                rowsDataPendidikan.map((data, i) => {
                                                    let e = i;
                                                    return (
                                                        <TableRow
                                                            key={i.toString()}
                                                            sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                                        >
                                                            <TableCell component="th" scope="row">
                                                                <TextField
                                                                    style={{ width: '100%' }}
                                                                    value={data?.name ?? data.name}
                                                                    onChange={(e) => handleChangeInputDetailPendidikan(e, i)}
                                                                    name='name'
                                                                    id="outlined-basic"
                                                                    label="Jenjang Pendidikan"
                                                                    variant="outlined"
                                                                />
                                                            </TableCell>
                                                            <TableCell component="th" scope="row">
                                                                <TextField
                                                                    style={{ width: '100%' }}
                                                                    value={data?.nama_institusi ?? data.nama_institusi}
                                                                    name='nama_institusi'
                                                                    id="outlined-basic"
                                                                    label="Nama Institusi"
                                                                    variant="outlined"
                                                                    onChange={(e) => handleChangeInputDetailPendidikan(e, i)}
                                                                />
                                                            </TableCell>
                                                            <TableCell>
                                                                <TextField
                                                                    style={{ width: '100%' }}
                                                                    value={data?.jurusan ?? data.jurusan}
                                                                    name='jurusan'
                                                                    id="outlined-basic"
                                                                    label="Jurusan"
                                                                    variant="outlined"
                                                                    onChange={(e) => handleChangeInputDetailPendidikan(e, i)}
                                                                />
                                                            </TableCell>
                                                            <TableCell>
                                                                <TextField
                                                                    style={{ width: '100%' }}
                                                                    value={data?.tahun_lulus ?? data.tahun_lulus}
                                                                    name='tahun_lulus'
                                                                    id="outlined-basic"
                                                                    label="Tahun Lulus"
                                                                    variant="outlined"
                                                                    onChange={(e) => handleChangeInputDetailPendidikan(e, i)}
                                                                />
                                                            </TableCell>
                                                            <TableCell>
                                                                <TextField
                                                                    style={{ width: '100%' }}
                                                                    value={data?.ipk ?? data.ipk}
                                                                    name='ipk'
                                                                    id="outlined-basic"
                                                                    label="IPK"
                                                                    variant="outlined"
                                                                    onChange={(e) => handleChangeInputDetailPendidikan(e, i)}
                                                                />
                                                            </TableCell>
                                                            <TableCell>
                                                                <Button
                                                                    title="Remove"
                                                                    color="error"
                                                                    variant="contained"
                                                                    onClick={() => handleDeleteRowsPendidikan(i)}
                                                                >
                                                                    Remove
                                                                </Button>
                                                            </TableCell>
                                                        </TableRow>
                                                    )
                                                })
                                            }
                                        </TableBody>
                                    </Table>
                                </TableContainer>
                            </Grid>
                        </Grid>
                    </CardContent>
                </Card>

                <Card style={{ marginTop: 10, marginBottom: 10, padding: 10 }}>
                    <CardContent style={{ padding: 10 }}>
                        <Typography mb={5} gutterBottom variant="h5" component="div">
                            Riwayat Pelatihan
                        </Typography>
                        <Grid item xs={12}>
                            <Button onClick={handleAddRowsPelatihan} style={{ marginTop: 10, marginRight: 10 }} variant="contained">Tambah Pelatihan</Button>
                            <TableContainer component={Paper} style={{ marginTop: 10 }}>
                                <Table sx={{ minWidth: 650 }} aria-label="simple table">
                                    <TableHead>
                                        <TableRow>
                                            <TableCell>Nama Kursus/Seminar</TableCell>
                                            <TableCell>Sertifikat (ada/tidak)</TableCell>
                                            <TableCell>Tahun</TableCell>
                                            <TableCell>Action</TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        {
                                            rowsDataPelatihan.map((data, i) => {
                                                let e = i;
                                                return (
                                                    <TableRow
                                                        key={i.toString()}
                                                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                                    >
                                                        <TableCell component="th" scope="row">
                                                            <TextField
                                                                style={{ width: '100%' }}
                                                                value={data?.name ?? data.name}
                                                                onChange={(e) => handleChangeInputDetailPelatihan(e, i)}
                                                                name='name'
                                                                id="outlined-basic"
                                                                label="Nama Kursus/Seminar"
                                                                variant="outlined"
                                                            />
                                                        </TableCell>
                                                        <TableCell component="th" scope="row">
                                                            <TextField
                                                                style={{ width: '100%' }}
                                                                value={data?.sertifikat ?? data.sertifikat}
                                                                name='sertifikat'
                                                                id="outlined-basic"
                                                                label="Sertifikat (ada/tidak)"
                                                                variant="outlined"
                                                                onChange={(e) => handleChangeInputDetailPelatihan(e, i)}
                                                            />
                                                        </TableCell>
                                                        <TableCell>
                                                            <TextField
                                                                style={{ width: '100%' }}
                                                                value={data?.tahun ?? data.tahun}
                                                                name='tahun'
                                                                id="outlined-basic"
                                                                label="Tahun"
                                                                variant="outlined"
                                                                onChange={(e) => handleChangeInputDetailPelatihan(e, i)}
                                                            />
                                                        </TableCell>
                                                        <TableCell>
                                                            <Button
                                                                title="Remove"
                                                                color="error"
                                                                variant="contained"
                                                                onClick={() => handleDeleteRowsPelatihan(i)}
                                                            >
                                                                Remove
                                                            </Button>
                                                        </TableCell>
                                                    </TableRow>
                                                )
                                            })
                                        }
                                    </TableBody>
                                </Table>
                            </TableContainer>
                        </Grid>
                    </CardContent>
                </Card>

                <Card style={{ marginTop: 10, marginBottom: 30, padding: 10 }}>
                    <CardContent style={{ padding: 10 }}>
                        <Typography mb={5} gutterBottom variant="h5" component="div">
                            Riwayat Pekerjaan
                        </Typography>
                        <Grid item xs={12}>
                            <Button onClick={handleAddRowsPekerjaan} style={{ marginTop: 10, marginRight: 10 }} variant="contained">Tambah Pekerjaan</Button>
                            <TableContainer component={Paper} style={{ marginTop: 10 }}>
                                <Table sx={{ minWidth: 650 }} aria-label="simple table">
                                    <TableHead>
                                        <TableRow>
                                            <TableCell>Nama Perusahaan</TableCell>
                                            <TableCell>Posisi Terakhir</TableCell>
                                            <TableCell>Pendapatan Terakhir</TableCell>
                                            <TableCell>Tahun</TableCell>
                                            <TableCell>Action</TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        {
                                            rowsDataPekerjaan.map((data, i) => {
                                                let e = i;
                                                return (
                                                    <TableRow
                                                        key={i.toString()}
                                                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                                    >
                                                        <TableCell component="th" scope="row">
                                                            <TextField
                                                                style={{ width: '100%' }}
                                                                value={data?.name ?? data.name}
                                                                onChange={(e) => handleChangeInputDetailPekerjaan(e, i)}
                                                                name='name'
                                                                id="outlined-basic"
                                                                label="Nama Perusahaan"
                                                                variant="outlined"
                                                            />
                                                        </TableCell>
                                                        <TableCell component="th" scope="row">
                                                            <TextField
                                                                style={{ width: '100%' }}
                                                                value={data?.posisi_terakhir ?? data.posisi_terakhir}
                                                                name='posisi_terakhir'
                                                                id="outlined-basic"
                                                                label="Posisi Terakhir"
                                                                variant="outlined"
                                                                onChange={(e) => handleChangeInputDetailPekerjaan(e, i)}
                                                            />
                                                        </TableCell>
                                                        <TableCell>
                                                            <TextField
                                                                style={{ width: '100%' }}
                                                                value={data?.pendapatan_terakhir ?? data.pendapatan_terakhir}
                                                                name='pendapatan_terakhir'
                                                                id="outlined-basic"
                                                                label="Pendapatan Terakhir"
                                                                variant="outlined"
                                                                onChange={(e) => handleChangeInputDetailPekerjaan(e, i)}
                                                            />
                                                        </TableCell>
                                                        <TableCell>
                                                            <TextField
                                                                style={{ width: '100%' }}
                                                                value={data?.tahun ?? data.tahun}
                                                                name='tahun'
                                                                id="outlined-basic"
                                                                label="Tahun"
                                                                variant="outlined"
                                                                onChange={(e) => handleChangeInputDetailPekerjaan(e, i)}
                                                            />
                                                        </TableCell>
                                                        <TableCell>
                                                            <Button
                                                                title="Remove"
                                                                color="error"
                                                                variant="contained"
                                                                onClick={() => handleDeleteRowsPekerjaan(i)}
                                                            >
                                                                Remove
                                                            </Button>
                                                        </TableCell>
                                                    </TableRow>
                                                )
                                            })
                                        }
                                    </TableBody>
                                </Table>
                            </TableContainer>
                        </Grid>
                        <Grid item xs={4}>
                            <Button onClick={handleSubmit} style={{ marginTop: 20, marginLeft: 10, float: 'right' }} variant="contained">Save</Button>
                            <Button href="/karyawan" color='inherit' style={{ marginTop: 20, float: 'right' }} variant="contained">Back</Button>
                        </Grid>
                    </CardContent>
                </Card>
            </form>
        </Container >
    )
}
