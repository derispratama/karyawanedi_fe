import React, { useEffect, useState } from 'react'
import { Card, CardContent, Checkbox, Container, FormControl, FormControlLabel, FormGroup, FormLabel, Grid, InputLabel, MenuItem, Paper, Radio, RadioGroup, Select, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, TextField, Typography } from '@mui/material'
import { DemoContainer } from '@mui/x-date-pickers/internals/demo';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import Button from '@mui/material/Button';
import { styled } from '@mui/material/styles';
import { useNavigate, useParams } from 'react-router-dom';
import dayjs from 'dayjs';
import Swal from 'sweetalert2'
import axios from 'axios';

export default function ShowKaryawan() {
    const token = localStorage.getItem("token");
    const email = localStorage.getItem("email");
    const navigate = useNavigate();
    const { id } = useParams();
    let [rowsDataPendidikan, setRowsDataPendidikan] = useState([]);
    let [rowsDataPelatihan, setRowsDataPelatihan] = useState([]);
    let [rowsDataPekerjaan, setRowsDataPekerjaan] = useState([]);

    const [datas, setDatas] = useState({
        posisi: '',
        name: '',
        ktp: '',
        email: email,
        tempat: '',
        tgl_lahir: dayjs(),
        jk: 'laki-laki',
        telp: '',
        penghasial_diharapkan: '',
        agama: 'islam',
        alamat_ktp: '',
        alamat_tinggal: '',
        status: '',
        skill: '',
        penghasial_diharapkan: '',
        gol_darah: 'A',
        org_dekat: '',
        bersedia: '',
        jenjang_pendidikan: {
            id: [],
            name: [],
            nama_institusi: [],
            jurusan: [],
            tahun_lulus: [],
            ipk: [],
        },
        riwayat_pelatihan: {
            id: [],
            name: [],
            sertifikat: [],
            tahun: [],
        },
        riwayat_pekerjaan: {
            id: [],
            name: [],
            posisi_terakhir: [],
            pendapatan_terakhir: [],
            tahun: [],
        }
    });

    const [error, setError] = useState({
        posisi: {
            error: false,
            message: '',
        },
        penghasial_diharapkan: {
            error: false,
            message: '',
        },
        gol_darah: {
            error: false,
            message: '',
        },
        org_dekat: {
            error: false,
            message: '',
        },
        name: {
            error: false,
            message: '',
        },
        ktp: {
            error: false,
            message: '',
        },
        email: {
            error: false,
            message: '',
        },
        tempat:
        {
            error: false,
            message: '',
        },
        tgl_lahir:
        {
            error: false,
            message: '',
        },
        jk:
        {
            error: false,
            message: '',
        },
        telp:
        {
            error: false,
            message: '',
        },
        agama:
        {
            error: false,
            message: '',
        },
        alamat_ktp:
        {
            error: false,
            message: '',
        },
        alamat_tinggal:
        {
            error: false,
            message: '',
        },
        status:
        {
            error: false,
            message: '',
        },
        skill: {
            error: false,
            message: '',
        },
        bersedia: {
            error: false,
            message: '',
        },
    });

    const handleChange = (e) => {
        const { name, value, type, checked } = e.target;

        if (type == 'checkbox') {
            if (checked) {
                setDatas(values => ({ ...values, [name]: 'YA' }));
            } else {
                setDatas(values => ({ ...values, [name]: 'TIDAK' }));
            }
        } else {
            setDatas(values => ({ ...values, [name]: value }));
        }

    }

    const handleSubmit = (e) => {
        e.preventDefault();
        Swal.fire({
            title: "Do you want to save the changes?",
            showCancelButton: true,
            icon: "warning",
            confirmButtonText: "Save",
        }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            Swal.showLoading();
            if (result.isConfirmed) {
                requestApi()
            }
        });
    }

    const requestApi = async () => {
        const data = {
            'id_user': id,
            'name': datas.name,
            'ktp': datas.ktp,
            'email': datas.email,
            'tempat': datas.tempat,
            'jk': datas.jk,
            'telp': datas.telp,
            'alamat_tinggal': datas.alamat_tinggal,
            'alamat_ktp': datas.alamat_ktp,
            'posisi': datas.posisi,
            'agama': datas.agama,
            'status': datas.status,
            'skill': datas.skill,
            'penghasial_diharapkan': datas.penghasial_diharapkan,
            'gol_darah': datas.gol_darah,
            'org_dekat': datas.org_dekat,
            'bersedia': datas.bersedia,
            'tgl_lahir': dayjs(datas.tgl_lahir).format('YYYY-MM-DD'),
            'jenjang_pendidikan': datas.jenjang_pendidikan,
            'riwayat_pelatihan': datas.riwayat_pelatihan,
            'riwayat_pekerjaan': datas.riwayat_pekerjaan,
        }


        await axios.post(`http://127.0.0.1:8000/api/karyawan/${id}`, data, {
            headers: {
                'accept': 'application/json',
                'Accept-Language': 'en-US,en;q=0.8',
                'Authorization': `Bearer ${token}`
            }
        })
            .then((res) => {
                Swal.hideLoading()
                Swal.fire("Edited!", "", "success");
                navigate('/karyawan/' + id + '/edit');
            })
            .catch((err) => {
                Swal.hideLoading()
                let errorku = { ...error };
                for (const errs in errorku) {
                    errorku[errs]['error'] = false;
                    errorku[errs]['message'] = '';
                }

                for (const errs in err.response.data.errors) {
                    if (errorku[errs]) {
                        errorku[errs]['error'] = true;
                        errorku[errs]['message'] = err.response.data.errors[errs][0];
                    }
                }
                setError(errorku)
            });
    }

    const getUser = async (id) => {
        await axios.get(`http://127.0.0.1:8000/api/karyawan/${id}`, {
            headers: {
                'accept': 'application/json',
                'Accept-Language': 'en-US,en;q=0.8',
                'Authorization': `Bearer ${token}`
            }
        })
            .then((res) => {
                let dt = { ...datas };
                dt['posisi'] = res.data.data.posisi;
                dt['ktp'] = res.data.data.ktp;
                dt['email'] = res.data.data.email;
                dt['ktp'] = res.data.data.ktp;
                dt['name'] = res.data.data.name;
                dt['tempat'] = res.data.data.tempat;
                dt['tgl_lahir'] = dayjs(res.data.data.tgl_lahir);
                dt['jk'] = res.data.data.jk;
                dt['telp'] = res.data.data.telp;
                dt['agama'] = res.data.data.agama;
                dt['alamat_ktp'] = res.data.data.alamat_ktp;
                dt['alamat_tinggal'] = res.data.data.alamat_tinggal;
                dt['status'] = res.data.data.status;
                dt['skill'] = res.data.data.skill;
                dt['gol_darah'] = res.data.data.gol_darah;
                dt['bersedia'] = res.data.data.bersedia;
                dt['org_dekat'] = res.data.data.org_dekat;
                dt['penghasial_diharapkan'] = res.data.data.penghasial_diharapkan;



                for (let i = 0; i < res.data.data.jenjang_pendidikan.length; i++) {
                    dt['jenjang_pendidikan'].id[i] = res.data.data.jenjang_pendidikan[i].id;
                    dt['jenjang_pendidikan'].name[i] = res.data.data.jenjang_pendidikan[i].name;
                    dt['jenjang_pendidikan'].nama_institusi[i] = res.data.data.jenjang_pendidikan[i].nama_institusi;
                    dt['jenjang_pendidikan'].jurusan[i] = res.data.data.jenjang_pendidikan[i].jurusan;
                    dt['jenjang_pendidikan'].tahun_lulus[i] = res.data.data.jenjang_pendidikan[i].tahun_lulus;
                    dt['jenjang_pendidikan'].ipk[i] = res.data.data.jenjang_pendidikan[i].ipk;
                }

                for (let i = 0; i < res.data.data.riwayat_pelatihan.length; i++) {
                    dt['riwayat_pelatihan'].id[i] = res.data.data.riwayat_pelatihan[i].id;
                    dt['riwayat_pelatihan'].name[i] = res.data.data.riwayat_pelatihan[i].name;
                    dt['riwayat_pelatihan'].sertifikat[i] = res.data.data.riwayat_pelatihan[i].sertifikat;
                    dt['riwayat_pelatihan'].tahun[i] = res.data.data.riwayat_pelatihan[i].tahun;
                }

                for (let i = 0; i < res.data.data.riwayat_pekerjaan.length; i++) {
                    dt['riwayat_pekerjaan'].id[i] = res.data.data.riwayat_pekerjaan[i].id;
                    dt['riwayat_pekerjaan'].name[i] = res.data.data.riwayat_pekerjaan[i].name;
                    dt['riwayat_pekerjaan'].posisi_terakhir[i] = res.data.data.riwayat_pekerjaan[i].posisi_terakhir;
                    dt['riwayat_pekerjaan'].pendapatan_terakhir[i] = res.data.data.riwayat_pekerjaan[i].pendapatan_terakhir;
                    dt['riwayat_pekerjaan'].tahun[i] = res.data.data.riwayat_pekerjaan[i].tahun;
                }

                setDatas(dt);

                let rdatapd = [...rowsDataPendidikan];
                let rdatapl = [...rowsDataPelatihan];
                let rdatapj = [...rowsDataPekerjaan];
                for (let j = 0; j < res.data.data.jenjang_pendidikan.length; j++) {
                    rdatapd.push({
                        id: res.data.data.jenjang_pendidikan[j].id,
                        name: res.data.data.jenjang_pendidikan[j].name,
                        nama_institusi: res.data.data.jenjang_pendidikan[j].nama_institusi,
                        jurusan: res.data.data.jenjang_pendidikan[j].jurusan,
                        tahun_lulus: res.data.data.jenjang_pendidikan[j].tahun_lulus,
                        ipk: res.data.data.jenjang_pendidikan[j].ipk,
                    });
                }

                for (let j = 0; j < res.data.data.riwayat_pelatihan.length; j++) {
                    rdatapl.push({
                        id: res.data.data.riwayat_pelatihan[j].id,
                        name: res.data.data.riwayat_pelatihan[j].name,
                        sertifikat: res.data.data.riwayat_pelatihan[j].sertifikat,
                        tahun: res.data.data.riwayat_pelatihan[j].tahun,
                    });
                }

                for (let j = 0; j < res.data.data.riwayat_pekerjaan.length; j++) {
                    rdatapj.push({
                        id: res.data.data.riwayat_pekerjaan[j].id,
                        name: res.data.data.riwayat_pekerjaan[j].name,
                        posisi_terakhir: res.data.data.riwayat_pekerjaan[j].posisi_terakhir,
                        pendapatan_terakhir: res.data.data.riwayat_pekerjaan[j].pendapatan_terakhir,
                        tahun: res.data.data.riwayat_pekerjaan[j].tahun,
                    });
                }

                setRowsDataPendidikan(rdatapd);
                setRowsDataPelatihan(rdatapl);
                setRowsDataPekerjaan(rdatapj);
            }).catch((err) => {
                console.error(err);
            });
    }

    const handleLogout = async () => {
        await axios.post(`http://127.0.0.1:8000/api/logout`, {
            headers: {
                'accept': 'application/json',
                'Accept-Language': 'en-US,en;q=0.8',
                'Authorization': `Bearer ${token}`
            }
        })
            .then((res) => {
                localStorage.removeItem('token')
                localStorage.removeItem('email')
                localStorage.removeItem('id')
                Swal.hideLoading()
                Swal.fire("Logout!", "", "success");
                navigate('/');
            })
            .catch((err) => {
                console.error(err);
            });
    }

    const handleAddRowsPendidikan = () => {
        setRowsDataPendidikan([
            ...rowsDataPendidikan,
            { id: '', name: '', nama_institusi: '', jurusan: '', tahun_lulus: '', ipk: '' }
        ]);
        const data = { ...datas };

        data.jenjang_pendidikan.id.push('');
        data.jenjang_pendidikan.name.push('');
        data.jenjang_pendidikan.nama_institusi.push('');
        data.jenjang_pendidikan.jurusan.push('');
        data.jenjang_pendidikan.tahun_lulus.push('');
        data.jenjang_pendidikan.ipk.push('');

        setDatas(data);
    };

    const handleAddRowsPelatihan = () => {
        setRowsDataPelatihan([
            ...rowsDataPelatihan,
            { id: '', name: '', sertifikat: '', tahun: '' }
        ]);
        const data = { ...datas };

        data.riwayat_pelatihan.id.push('');
        data.riwayat_pelatihan.name.push('');
        data.riwayat_pelatihan.sertifikat.push('');
        data.riwayat_pelatihan.tahun.push('');

        setDatas(data);
    };

    const handleAddRowsPekerjaan = () => {
        setRowsDataPekerjaan([
            ...rowsDataPekerjaan,
            { id: '', name: '', posisi_terakhir: '', pendapatan_terakhir: '', tahun: '' }
        ]);
        const data = { ...datas };

        data.riwayat_pekerjaan.id.push('');
        data.riwayat_pekerjaan.name.push('');
        data.riwayat_pekerjaan.posisi_terakhir.push('');
        data.riwayat_pekerjaan.pendapatan_terakhir.push('');
        data.riwayat_pekerjaan.tahun.push('');

        setDatas(data);
    };

    const handleDeleteRowsPendidikan = (i) => {
        const deleteVal = [...rowsDataPendidikan];
        const post = { ...datas };
        if (i === 'last') {
            deleteVal.pop();
            post.jenjang_pendidikan.id.pop();
            post.jenjang_pendidikan.name.pop();
            post.jenjang_pendidikan.nama_institusi.pop();
            post.jenjang_pendidikan.jurusan.pop();
            post.jenjang_pendidikan.tahun_lulus.pop();
            post.jenjang_pendidikan.ipk.pop();
        } else {
            deleteVal.splice(i, 1);
            post.jenjang_pendidikan.id.splice(i, 1);
            post.jenjang_pendidikan.name.splice(i, 1);
            post.jenjang_pendidikan.nama_institusi.splice(i, 1);
            post.jenjang_pendidikan.jurusan.splice(i, 1);
            post.jenjang_pendidikan.tahun_lulus.splice(i, 1);
            post.jenjang_pendidikan.ipk.splice(i, 1);
        }
        setRowsDataPendidikan(deleteVal);
        setDatas(post);
    }

    const handleDeleteRowsPelatihan = (i) => {
        const deleteVal = [...rowsDataPelatihan];
        const post = { ...datas };
        if (i === 'last') {
            deleteVal.pop();
            post.riwayat_pelatihan.id.pop();
            post.riwayat_pelatihan.name.pop();
            post.riwayat_pelatihan.sertifikat.pop();
            post.riwayat_pelatihan.tahun.pop();
        } else {
            deleteVal.splice(i, 1);
            post.riwayat_pelatihan.id.splice(i, 1);
            post.riwayat_pelatihan.name.splice(i, 1);
            post.riwayat_pelatihan.sertifikat.splice(i, 1);
            post.riwayat_pelatihan.tahun.splice(i, 1);
        }
        setRowsDataPelatihan(deleteVal);
        setDatas(post);
    }

    const handleDeleteRowsPekerjaan = (i) => {
        const deleteVal = [...rowsDataPekerjaan];
        const post = { ...datas };
        if (i === 'last') {
            deleteVal.pop();
            post.riwayat_pekerjaan.id.pop();
            post.riwayat_pekerjaan.name.pop();
            post.riwayat_pekerjaan.posisi_terakhir.pop();
            post.riwayat_pekerjaan.pendapatan_terakhir.pop();
            post.riwayat_pekerjaan.tahun.pop();
        } else {
            deleteVal.splice(i, 1);
            post.riwayat_pekerjaan.id.splice(i, 1);
            post.riwayat_pekerjaan.name.splice(i, 1);
            post.riwayat_pekerjaan.posisi_terakhir.splice(i, 1);
            post.riwayat_pekerjaan.pendapatan_terakhir.splice(i, 1);
            post.riwayat_pekerjaan.tahun.splice(i, 1);
        }
        setRowsDataPekerjaan(deleteVal);
        setDatas(post);
    }

    const handleChangeInputDetailPendidikan = (e, index) => {
        const { name, value } = e.target;
        const post = { ...datas };
        const datap = [...rowsDataPendidikan];

        post.jenjang_pendidikan[name][index] = value;
        datap[index][name] = value;

        setDatas(post);
        setRowsDataPendidikan(datap);
    }

    const handleChangeInputDetailPelatihan = (e, index) => {
        const { name, value } = e.target;
        const post = { ...datas };
        const datap = [...rowsDataPelatihan];

        post.riwayat_pelatihan[name][index] = value;
        datap[index][name] = value;

        setDatas(post);
        setRowsDataPelatihan(datap);
    }

    const handleChangeInputDetailPekerjaan = (e, index) => {
        const { name, value } = e.target;
        const post = { ...datas };
        const datap = [...rowsDataPekerjaan];

        post.riwayat_pekerjaan[name][index] = value;
        datap[index][name] = value;

        setDatas(post);
        setRowsDataPekerjaan(datap);
    }

    useEffect(() => {
        getUser(id);
    }, []);

    return (
        <Container>
            <form noValidate autoComplete='off' onSubmit={handleSubmit}>

                <Card style={{ marginTop: 100, marginBottom: 100, padding: 10 }}>
                    <CardContent style={{ padding: 10 }}>
                        <Grid container spacing={2}>
                            <Grid item xs={6}>
                                <Typography mb={5} gutterBottom variant="h5" component="div">
                                    Data Pribadi Pelamar
                                </Typography>
                            </Grid>
                            <Grid item xs={6}>
                                <Button onClick={handleLogout} style={{ float: 'right', marginBottom: 20, marginRight: 10, textAlign: 'center' }} variant="contained">Logout</Button>
                            </Grid>
                        </Grid>

                        <Grid container spacing={2}>
                            <Grid item xs={6}>
                                <TextField
                                    style={{ width: '100%' }}
                                    error={error.posisi.error}
                                    name='posisi'
                                    value={datas.posisi}
                                    id="outlined-basic"
                                    label="Posisi Yang Dilamar"
                                    variant="outlined"
                                    helperText={error.posisi.message}
                                    onChange={handleChange}
                                />
                            </Grid>
                            <Grid item xs={6}>
                                <TextField
                                    style={{ width: '100%' }}
                                    error={error.name.error}
                                    value={datas.name}
                                    name='name'
                                    id="outlined-basic"
                                    label="Nama"
                                    variant="outlined"
                                    helperText={error.name.message}
                                    onChange={handleChange}
                                />
                            </Grid>
                            <Grid item xs={6}>
                                <TextField
                                    onKeyPress={(event) => {
                                        if (!/[0-9]/.test(event.key)) {
                                            event.preventDefault();
                                        }
                                    }}
                                    style={{ width: '100%' }}
                                    value={datas.ktp}
                                    error={error.ktp.error}
                                    name='ktp'
                                    id="outlined-basic"
                                    label="No KTP"
                                    variant="outlined"
                                    helperText={error.ktp.message}
                                    onChange={handleChange}
                                />
                            </Grid>
                            <Grid item xs={6}>
                                <TextField
                                    style={{ width: '100%' }}
                                    value={datas.status}
                                    error={error.status.error}
                                    name='status'
                                    id="outlined-basic"
                                    label="Status"
                                    variant="outlined"
                                    helperText={error.status.message}
                                    onChange={handleChange}
                                />
                            </Grid>
                            <Grid item xs={8}>
                                <TextField
                                    style={{ width: '100%' }}
                                    error={error.tempat.error}
                                    value={datas.tempat}
                                    name='tempat'
                                    id="outlined-basic"
                                    label="Tempat Lahir"
                                    variant="outlined"
                                    helperText={error.tempat.message}
                                    onChange={handleChange}
                                />
                            </Grid>
                            <Grid item xs={4}>
                                <LocalizationProvider dateAdapter={AdapterDayjs} >
                                    <DemoContainer components={['DatePicker']} sx={{ marginTop: -1, width: '100%' }}>
                                        <DatePicker
                                            label="Tanggal Lahir"
                                            name='tgl_lahir'
                                            value={datas.tgl_lahir}
                                            onChange={(v) => setDatas(values => ({ ...values, 'tgl_lahir': v }))}
                                        />
                                    </DemoContainer>
                                </LocalizationProvider>
                            </Grid>
                            <Grid item xs={6}>
                                <FormControl fullWidth>
                                    <InputLabel id="demo-simple-select-label">Agama</InputLabel>
                                    <Select
                                        labelId="demo-simple-select-label"
                                        id="demo-simple-select"
                                        name="agama"
                                        value={datas.agama}
                                        label="Agama"
                                        onChange={handleChange}
                                    >
                                        <MenuItem value={'islam'}>Islam</MenuItem>
                                        <MenuItem value={'kristen'}>Kristen</MenuItem>
                                        <MenuItem value={'hindu'}>Hindu</MenuItem>
                                    </Select>
                                    {
                                        error.agama.error == true && (
                                            <Typography color={'error'} style={{ fontSize: 12 }}>
                                                {error.agama.message}
                                            </Typography>
                                        )
                                    }
                                </FormControl>
                            </Grid>
                            <Grid item xs={6}>
                                <FormControl>
                                    <FormLabel id="demo-radio-buttons-group-label">Jenis Kelamin</FormLabel>
                                    <RadioGroup
                                        aria-labelledby="demo-radio-buttons-group-label"
                                        defaultValue="laki-laki"
                                        name="jk"
                                        value={datas.jk}
                                        onChange={handleChange}
                                        row
                                    >
                                        <FormControlLabel value="laki-laki" control={<Radio />} label="Laki-laki" />
                                        <FormControlLabel value="perempuan" control={<Radio />} label="Perempuan" />
                                    </RadioGroup>
                                </FormControl>
                            </Grid>
                            <Grid item xs={6}>
                                <TextField
                                    error={error.alamat_ktp.error}
                                    helperText={error.alamat_ktp.message}
                                    value={datas.alamat_ktp}
                                    style={{ width: '100%' }}
                                    id="outlined-multiline-static"
                                    label="Alamat KTP"
                                    name='alamat'
                                    multiline
                                    rows={4}
                                    onChange={handleChange}
                                />
                            </Grid>
                            <Grid item xs={6}>
                                <TextField
                                    error={error.alamat_tinggal.error}
                                    helperText={error.alamat_tinggal.message}
                                    value={datas.alamat_tinggal}
                                    style={{ width: '100%' }}
                                    id="outlined-multiline-static"
                                    label="Alamat Tinggal"
                                    name='alamat'
                                    multiline
                                    rows={4}
                                    onChange={handleChange}
                                />
                            </Grid>
                            <Grid item xs={6}>
                                <TextField
                                    style={{ width: '100%' }}
                                    error={error.email.error}
                                    value={datas.email}
                                    name='email'
                                    id="outlined-basic"
                                    label="Email"
                                    variant="outlined"
                                    helperText={error.email.message}
                                    onChange={handleChange}
                                />
                            </Grid>
                            <Grid item xs={6}>
                                <TextField
                                    onKeyPress={(event) => {
                                        if (!/[0-9]/.test(event.key)) {
                                            event.preventDefault();
                                        }
                                    }}
                                    style={{ width: '100%' }}
                                    error={error.telp.error}
                                    value={datas.telp}
                                    helperText={error.telp.message}
                                    name='telp'
                                    id="outlined-basic"
                                    label="No Telp/HP"
                                    variant="outlined"
                                    onChange={handleChange} />
                            </Grid>
                            <Grid item xs={6}>
                                <TextField
                                    style={{ width: '100%' }}
                                    error={error.status.error}
                                    value={datas.status}
                                    name='status'
                                    id="outlined-basic"
                                    label="Orang Yang Dapat Dihubungi"
                                    variant="outlined"
                                    helperText={error.status.message}
                                    onChange={handleChange}
                                />
                            </Grid>
                            <Grid item xs={6}>
                                <FormControl fullWidth>
                                    <InputLabel id="demo-simple-select-label">Golongan Darah</InputLabel>
                                    <Select
                                        labelId="demo-simple-select-label"
                                        id="demo-simple-select"
                                        name="gol_darah"
                                        value={datas.gol_darah}
                                        label="Golongan Darah"
                                        onChange={handleChange}
                                    >
                                        <MenuItem value={'O'}>O</MenuItem>
                                        <MenuItem value={'A'}>A</MenuItem>
                                        <MenuItem value={'AB'}>AB</MenuItem>
                                        <MenuItem value={'B'}>B</MenuItem>
                                    </Select>
                                    {
                                        error.gol_darah.error == true && (
                                            <Typography color={'error'} style={{ fontSize: 12 }}>
                                                {error.gol_darah.message}
                                            </Typography>
                                        )
                                    }
                                </FormControl>
                            </Grid>
                            <Grid item xs={6}>
                                <TextField
                                    onKeyPress={(event) => {
                                        if (!/[0-9]/.test(event.key)) {
                                            event.preventDefault();
                                        }
                                    }}
                                    style={{ width: '100%' }}
                                    error={error.penghasial_diharapkan.error}
                                    value={datas.penghasial_diharapkan}
                                    helperText={error.penghasial_diharapkan.message}
                                    name='penghasial_diharapkan'
                                    id="outlined-basic"
                                    label="Penghasilan Diharapkan"
                                    variant="outlined"
                                    onChange={handleChange} />
                            </Grid>
                            <Grid item xs={12}>
                                <InputLabel id="demo-simple-select-label">Bersedia Ditempatkan Di Seluruh Kantor Perusahaan</InputLabel>
                                <FormGroup row>
                                    <FormControlLabel control={<Checkbox name="bersedia" checked={datas.bersedia == 'YA' ? true : false} value='YA' onChange={handleChange} />} label="Saya bersedia" />
                                </FormGroup>
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    error={error.skill.error}
                                    helperText={error.skill.message}
                                    value={datas.skill}
                                    style={{ width: '100%' }}
                                    id="outlined-multiline-static"
                                    label="Skill"
                                    name='skill'
                                    multiline
                                    rows={4}
                                    onChange={handleChange}
                                />
                            </Grid>
                        </Grid>
                    </CardContent>
                </Card>
                <Card style={{ marginTop: 10, marginBottom: 10, padding: 10 }}>
                    <CardContent style={{ padding: 10 }}>
                        <Typography mb={5} gutterBottom variant="h5" component="div">
                            Pendidikan Terakhir
                        </Typography>
                        <Grid container spacing={2}>
                            <Grid item xs={12}>
                                <TableContainer component={Paper} style={{ marginTop: 10 }}>
                                    <Table sx={{ minWidth: 650 }} aria-label="simple table">
                                        <TableHead>
                                            <TableRow>
                                                <TableCell>Jenjang Pendidikan</TableCell>
                                                <TableCell>Nama Instusi</TableCell>
                                                <TableCell>Jurusan</TableCell>
                                                <TableCell>Tahun Lulus</TableCell>
                                                <TableCell>IPK</TableCell>
                                                <TableCell>Action</TableCell>
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>
                                            {
                                                rowsDataPendidikan.map((data, i) => {
                                                    let e = i;
                                                    return (
                                                        <TableRow
                                                            key={i.toString()}
                                                            sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                                        >
                                                            <TableCell component="th" scope="row">
                                                                <TextField
                                                                    style={{ width: '100%' }}
                                                                    value={data?.name ?? data.name}
                                                                    onChange={(e) => handleChangeInputDetailPendidikan(e, i)}
                                                                    name='name'
                                                                    id="outlined-basic"
                                                                    label="Jenjang Pendidikan"
                                                                    variant="outlined"
                                                                />
                                                            </TableCell>
                                                            <TableCell component="th" scope="row">
                                                                <TextField
                                                                    style={{ width: '100%' }}
                                                                    value={data?.nama_institusi ?? data.nama_institusi}
                                                                    name='nama_institusi'
                                                                    id="outlined-basic"
                                                                    label="Nama Institusi"
                                                                    variant="outlined"
                                                                    onChange={(e) => handleChangeInputDetailPendidikan(e, i)}
                                                                />
                                                            </TableCell>
                                                            <TableCell>
                                                                <TextField
                                                                    style={{ width: '100%' }}
                                                                    value={data?.jurusan ?? data.jurusan}
                                                                    name='jurusan'
                                                                    id="outlined-basic"
                                                                    label="Jurusan"
                                                                    variant="outlined"
                                                                    onChange={(e) => handleChangeInputDetailPendidikan(e, i)}
                                                                />
                                                            </TableCell>
                                                            <TableCell>
                                                                <TextField
                                                                    onKeyPress={(event) => {
                                                                        if (!/[0-9]/.test(event.key)) {
                                                                            event.preventDefault();
                                                                        }
                                                                    }}
                                                                    style={{ width: '100%' }}
                                                                    value={data?.tahun_lulus ?? data.tahun_lulus}
                                                                    name='tahun_lulus'
                                                                    id="outlined-basic"
                                                                    label="Tahun Lulus"
                                                                    variant="outlined"
                                                                    onChange={(e) => handleChangeInputDetailPendidikan(e, i)}
                                                                />
                                                            </TableCell>
                                                            <TableCell>
                                                                <TextField
                                                                    style={{ width: '100%' }}
                                                                    value={data?.ipk ?? data.ipk}
                                                                    name='ipk'
                                                                    id="outlined-basic"
                                                                    label="IPK"
                                                                    variant="outlined"
                                                                    onChange={(e) => handleChangeInputDetailPendidikan(e, i)}
                                                                />
                                                            </TableCell>
                                                            <TableCell>
                                                                -
                                                            </TableCell>
                                                        </TableRow>
                                                    )
                                                })
                                            }
                                        </TableBody>
                                    </Table>
                                </TableContainer>
                            </Grid>
                        </Grid>
                    </CardContent>
                </Card>

                <Card style={{ marginTop: 10, marginBottom: 10, padding: 10 }}>
                    <CardContent style={{ padding: 10 }}>
                        <Typography mb={5} gutterBottom variant="h5" component="div">
                            Riwayat Pelatihan
                        </Typography>
                        <Grid item xs={12}>
                            <TableContainer component={Paper} style={{ marginTop: 10 }}>
                                <Table sx={{ minWidth: 650 }} aria-label="simple table">
                                    <TableHead>
                                        <TableRow>
                                            <TableCell>Nama Kursus/Seminar</TableCell>
                                            <TableCell>Sertifikat (ada/tidak)</TableCell>
                                            <TableCell>Tahun</TableCell>
                                            <TableCell>Action</TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        {
                                            rowsDataPelatihan.map((data, i) => {
                                                let e = i;
                                                return (
                                                    <TableRow
                                                        key={i.toString()}
                                                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                                    >
                                                        <TableCell component="th" scope="row">
                                                            <TextField
                                                                style={{ width: '100%' }}
                                                                value={data?.name ?? data.name}
                                                                onChange={(e) => handleChangeInputDetailPelatihan(e, i)}
                                                                name='name'
                                                                id="outlined-basic"
                                                                label="Nama Kursus/Seminar"
                                                                variant="outlined"
                                                            />
                                                        </TableCell>
                                                        <TableCell component="th" scope="row">
                                                            <TextField
                                                                style={{ width: '100%' }}
                                                                value={data?.sertifikat ?? data.sertifikat}
                                                                name='sertifikat'
                                                                id="outlined-basic"
                                                                label="Sertifikat (ada/tidak)"
                                                                variant="outlined"
                                                                onChange={(e) => handleChangeInputDetailPelatihan(e, i)}
                                                            />
                                                        </TableCell>
                                                        <TableCell>
                                                            <TextField
                                                                onKeyPress={(event) => {
                                                                    if (!/[0-9]/.test(event.key)) {
                                                                        event.preventDefault();
                                                                    }
                                                                }}
                                                                style={{ width: '100%' }}
                                                                value={data?.tahun ?? data.tahun}
                                                                name='tahun'
                                                                id="outlined-basic"
                                                                label="Tahun"
                                                                variant="outlined"
                                                                onChange={(e) => handleChangeInputDetailPelatihan(e, i)}
                                                            />
                                                        </TableCell>
                                                        <TableCell>
                                                            -
                                                        </TableCell>
                                                    </TableRow>
                                                )
                                            })
                                        }
                                    </TableBody>
                                </Table>
                            </TableContainer>
                        </Grid>
                    </CardContent>
                </Card>

                <Card style={{ marginTop: 10, marginBottom: 30, padding: 10 }}>
                    <CardContent style={{ padding: 10 }}>
                        <Typography mb={5} gutterBottom variant="h5" component="div">
                            Riwayat Pekerjaan
                        </Typography>
                        <Grid item xs={12}>
                            <TableContainer component={Paper} style={{ marginTop: 10 }}>
                                <Table sx={{ minWidth: 650 }} aria-label="simple table">
                                    <TableHead>
                                        <TableRow>
                                            <TableCell>Nama Perusahaan</TableCell>
                                            <TableCell>Posisi Terakhir</TableCell>
                                            <TableCell>Pendapatan Terakhir</TableCell>
                                            <TableCell>Tahun</TableCell>
                                            <TableCell>Action</TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        {
                                            rowsDataPekerjaan.map((data, i) => {
                                                let e = i;
                                                return (
                                                    <TableRow
                                                        key={i.toString()}
                                                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                                    >
                                                        <TableCell component="th" scope="row">
                                                            <TextField
                                                                style={{ width: '100%' }}
                                                                value={data?.name ?? data.name}
                                                                onChange={(e) => handleChangeInputDetailPekerjaan(e, i)}
                                                                name='name'
                                                                id="outlined-basic"
                                                                label="Jenjang Pendidikan"
                                                                variant="outlined"
                                                            />
                                                        </TableCell>
                                                        <TableCell component="th" scope="row">
                                                            <TextField
                                                                style={{ width: '100%' }}
                                                                value={data?.posisi_terakhir ?? data.posisi_terakhir}
                                                                name='posisi_terakhir'
                                                                id="outlined-basic"
                                                                label="Nama Institusi"
                                                                variant="outlined"
                                                                onChange={(e) => handleChangeInputDetailPekerjaan(e, i)}
                                                            />
                                                        </TableCell>
                                                        <TableCell>
                                                            <TextField
                                                                onKeyPress={(event) => {
                                                                    if (!/[0-9]/.test(event.key)) {
                                                                        event.preventDefault();
                                                                    }
                                                                }}
                                                                style={{ width: '100%' }}
                                                                value={data?.pendapatan_terakhir ?? data.pendapatan_terakhir}
                                                                name='pendapatan_terakhir'
                                                                id="outlined-basic"
                                                                label="Pendapatan Terakhir"
                                                                variant="outlined"
                                                                onChange={(e) => handleChangeInputDetailPekerjaan(e, i)}
                                                            />
                                                        </TableCell>
                                                        <TableCell>
                                                            <TextField
                                                                style={{ width: '100%' }}
                                                                value={data?.tahun ?? data.tahun}
                                                                onKeyPress={(event) => {
                                                                    if (!/[0-9]/.test(event.key)) {
                                                                        event.preventDefault();
                                                                    }
                                                                }}
                                                                name='tahun'
                                                                id="outlined-basic"
                                                                label="Tahun"
                                                                variant="outlined"
                                                                onChange={(e) => handleChangeInputDetailPekerjaan(e, i)}
                                                            />
                                                        </TableCell>
                                                        <TableCell>
                                                            -
                                                        </TableCell>
                                                    </TableRow>
                                                )
                                            })
                                        }
                                    </TableBody>
                                </Table>
                            </TableContainer>
                        </Grid>
                        <Grid item xs={4}>
                            <Button href="/karyawan" color='inherit' style={{ marginTop: 20, float: 'right' }} variant="contained">Back</Button>
                        </Grid>
                    </CardContent>
                </Card>
            </form>

        </Container >
    )
}
