import { Button, Card, CardContent, Container, Grid, TextField, Typography } from '@mui/material'
import axios from 'axios';
import React, { useState } from 'react'
import { Link, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function Login() {
    const navigate = useNavigate();

    const [datas, setDatas] = useState({
        email: '',
        password: '',
    });

    const [error, setError] = useState({
        email: {
            error: false,
            message: '',
        },
        password: {
            error: false,
            message: '',
        },
    });

    const handleChange = (e) => {
        const { name, value } = e.target;
        setDatas(values => ({ ...values, [name]: value }));
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        requestApi();
    }

    const requestApi = async () => {
        const formData = new FormData();
        formData.append('email', datas.email);
        formData.append('password', datas.password);
        await axios.post(`http://127.0.0.1:8000/api/login`, formData, {
            headers: {
                'accept': 'application/json',
                'Accept-Language': 'en-US,en;q=0.8',
            }
        })
            .then((res) => {
                localStorage.setItem("token", res.data.token);
                localStorage.setItem("email", res.data.user.email);
                localStorage.setItem("role", res.data.user.role);
                if (res.data.user.karyawan[0]) {
                    localStorage.setItem("id", res.data.user.karyawan[0].id);
                } else {
                    localStorage.setItem("id", res.data.user.id);
                }
                localStorage.setItem("registered", 'yes');
                Swal.fire("Login!", "", "success");
                const ress = res.data.user.role;
                if (ress == 'ADMIN') {
                    navigate('/karyawan');
                } else {
                    navigate('/karyawan/' + res.data.user.karyawan[0].id + '/edit');
                }

            })
            .catch((err) => {
                console.log(err)
                Swal.hideLoading()
                let errorku = { ...error };
                for (const errs in errorku) {
                    errorku[errs]['error'] = false;
                    errorku[errs]['message'] = '';
                }

                for (const errs in err.response.data.errors) {
                    errorku[errs]['error'] = true;
                    errorku[errs]['message'] = err.response.data.errors[errs][0];
                }
                setError(errorku)
            });
    }

    return (
        <Container>
            <Card style={{ marginTop: 100, padding: 10 }}>
                <CardContent style={{ padding: 10 }}>
                    <Typography mb={5} gutterBottom variant="h5" component="div">
                        Login
                    </Typography>
                    <form noValidate autoComplete='off' onSubmit={handleSubmit}>
                        <Grid container spacing={3}>
                            <Grid item xs={12}>
                                <TextField
                                    style={{ width: '100%' }}
                                    error={error.email.error}
                                    name='email'
                                    value={datas.email}
                                    id="outlined-basic"
                                    label="Email"
                                    variant="outlined"
                                    helperText={error.email.message}
                                    onChange={handleChange}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    style={{ width: '100%' }}
                                    error={error.password.error}
                                    value={datas.password}
                                    name='password'
                                    type='password'
                                    id="outlined-basic"
                                    label="Password"
                                    variant="outlined"
                                    helperText={error.password.message}
                                    onChange={handleChange}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <Button onClick={handleSubmit} style={{ marginTop: 10, marginRight: 10, width: '100%', textAlign: 'center' }} variant="contained">Login</Button>
                                <Link to={'/register'}>Register</Link>
                            </Grid>
                        </Grid>
                    </form>
                </CardContent>
            </Card>
        </Container>
    )
}
