import React, { useEffect, useMemo, useState } from 'react'
import { DataGrid, GridToolbar } from '@mui/x-data-grid';
import { Button, Card, CardContent, Container, Grid, Typography } from '@mui/material';
import axios from 'axios';
import Swal from 'sweetalert2';
import { useNavigate } from 'react-router-dom';

export default function ListKaryawan() {
    const navigate = useNavigate();
    const [pageSize, setPageSize] = useState(5);
    const [datas, setDatas] = useState([]);

    const token = localStorage.getItem("token");
    const columns = useMemo(
        () => [
            { field: 'id', headerName: 'No', width: 100 },
            { field: 'name', headerName: 'Nama', width: 180 },
            { field: 'tempat', headerName: 'Tempat Tanggal Lahir', width: 180 },
            { field: 'posisi', headerName: 'Posisi', width: 180 },
            {
                field: 'action', headerName: 'Action', width: 300,
                renderCell: (params) => {
                    const handleDelete = (id) => {
                        Swal.fire({
                            title: "Do you want to delete this data?",
                            icon: "warning",
                            showCancelButton: true,
                            confirmButtonText: "Delete",
                        }).then((result) => {/* Read more about isConfirmed, isDenied below */
                            Swal.showLoading();
                            if (result.isConfirmed) {
                                requestApi(id)
                            }
                        });
                    }

                    return (
                        <>
                            <Button href={`/karyawan/${params.id}/show`} variant="contained" style={{ marginRight: 5 }}>Show</Button>
                            <Button href={`/karyawan/${params.id}/edit`} variant="contained" style={{ marginRight: 5 }}>Edit</Button>
                            <Button variant="contained" color='error' onClick={() => handleDelete(params.id)}>Delete</Button>
                        </>
                    )
                }
            },
        ],
        []
    );

    const requestApi = async (id) => {
        await axios.delete(`http://127.0.0.1:8000/api/karyawan/${id}`, {
            headers: {
                'accept': 'application/json',
                'Accept-Language': 'en-US,en;q=0.8',
                'Authorization': `Bearer ${token}`
            }
        })
            .then((res) => {
                Swal.hideLoading()
                Swal.fire("Deleted!", "", "success");
                getUser();
            })
            .catch((err) => {
                console.error(err);
            });
    }

    const getUser = async () => {
        await axios.get('http://127.0.0.1:8000/api/karyawan', {
            headers: {
                'accept': 'application/json',
                'Accept-Language': 'en-US,en;q=0.8',
                'Authorization': `Bearer ${token}`
            }
        })
            .then((res) => {
                setDatas(res.data.data);
            }).catch((err) => {
                console.error(err);
            });
    }

    useEffect(() => {
        getUser();
    }, [])

    const handleLogout = async () => {
        await axios.post(`http://127.0.0.1:8000/api/logout`, {
            headers: {
                'accept': 'application/json',
                'Accept-Language': 'en-US,en;q=0.8',
                'Authorization': `Bearer ${token}`
            }
        })
            .then((res) => {
                localStorage.removeItem('token')
                localStorage.removeItem('email')
                localStorage.removeItem('id')
                Swal.hideLoading()
                Swal.fire("Logout!", "", "success");
                navigate('/');
            })
            .catch((err) => {
                console.error(err);
            });
    }

    return (
        <Container>
            <Card style={{ marginTop: 100, padding: 10 }}>
                <Grid container spacing={2}>
                    <Grid item xs={6}>
                        <Typography mb={5} gutterBottom variant="h5" component="div">
                            Data Pribadi Pelamar
                        </Typography>
                    </Grid>
                    <Grid item xs={6}>
                        <Button onClick={handleLogout} style={{ float: 'right', marginBottom: 20, marginRight: 10, textAlign: 'center' }} variant="contained">Logout</Button>
                    </Grid>
                </Grid>
                <CardContent style={{ padding: 10 }}>
                    <DataGrid
                        disableColumnFilter
                        rows={datas}
                        columns={columns}
                        initialState={{
                            pagination: {
                                paginationModel: { page: 0, pageSize: 5 },
                            },
                        }}
                        slots={{ toolbar: GridToolbar }}
                        slotProps={{
                            toolbar: {
                                showQuickFilter: true,
                            },
                        }}
                        pageSizeOptions={[5, 10]}
                        pageSize={pageSize}
                        onPageSizeChange={(newPageSize) => setPageSize(newPageSize)}
                        getRowSpacing={params => ({
                            top: params.isFirstVisible ? 0 : 5,
                            bottom: params.isLastVisible ? 0 : 5,
                        })}
                    />
                </CardContent>
            </Card>
        </Container>
    )
}
